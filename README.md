# AssignmentTerraform



# How to deploy a three-tier architecture in AWS using Terraform?


### Prerequisites:


* AWS account
* AWS Access & Secret Key


**Step 1:- Create a file for the Provider**
Create provider.tf file and add the below code to it
```
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
}

```
**Step 2:- Create a file for the backend**

* Create backend.tf file and add the below code to it
```
terraform {
  backend "s3" {
    bucket         = "myfstbcktposwal"
    key            = "terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terraform-lock-table"
  }
}

```

**Step 3:- Create a file for the VPC**

* Create vpc.tf file and add the below code to it

  ```
  # Creating VPC
  resource "aws_vpc" "demovpc" {
    cidr_block       = "${var.vpc_cidr}"
    instance_tenancy = "default"
  tags = {
    Name = "Demo VPC"
  }
  }
  ```
  
**Step 4:- Create a file for the Subnet**

* For this project, I will create total 2 public subnets for the front-end tier 
* Create subnet.tf file and add the below code to it

  ```
  # Creating 1st web subnet 
  resource "aws_subnet" "public-subnet-1" {
    vpc_id                  = "${aws_vpc.demovpc.id}"
    cidr_block             = "${var.subnet_cidr}"
    map_public_ip_on_launch = true
    availability_zone = "us-east-1a"
  tags = {
    Name = "Web Subnet 1"
  }
  }
  # Creating 2nd web subnet 
  resource "aws_subnet" "public-subnet-2" {
    vpc_id                  = "${aws_vpc.demovpc.id}"
    cidr_block             = "${var.subnet1_cidr}"
    map_public_ip_on_launch = true
    availability_zone = "us-east-1b"
  tags = {
    Name = "Web Subnet 2"
  }
  }
  
  ```
  
**Step 5:- Create a file for the Internet Gateway**

* Create igw.tf file and add the below code to it

  ```
  # Creating Internet Gateway 
  resource "aws_internet_gateway" "demogateway" {
    vpc_id = "${aws_vpc.demovpc.id}"
  }
  ```

**Step 6:- Create a file for the Route table**

* Create route_table_public.tf file and add the below code to it

  ```
  # Creating Route Table
  resource "aws_route_table" "route" {
    vpc_id = "${aws_vpc.demovpc.id}"
  route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.demogateway.id}"
    }
  tags = {
        Name = "Route to internet"
    }
  }
  # Associating Route Table
  resource "aws_route_table_association" "rt1" {
    subnet_id = "${aws_subnet.public-subnet-1.id}"
    route_table_id = "${aws_route_table.route.id}"
  }
  # Associating Route Table
  resource "aws_route_table_association" "rt2" {
    subnet_id = "${aws_subnet.public-subnet-2.id}"
    route_table_id = "${aws_route_table.route.id}"
  }
  ```
* In the above code, I am creating a new route table and forwarding all the requests to the 0.0.0.0/0 CIDR block.
* I am also attaching this route table to the subnet created earlier. So, it will work as the Public Subnet


**Step 7:- Create a file for IAM_policy and IAM Role for Systems Manager and Attach the IAM Policy to the IAM Role**

```
# IAM Policy for Systems Manager with additional statements
resource "aws_iam_policy" "ssm_policy" {
  name        = "SSMPolicy"
  description = "Policy for AWS Systems Manager with additional statements"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect    = "Allow",
        Action    = [
          "ssm:Put*",
          "ssm:Get*",
          "ssm:List*",
          "ssm:UpdateInstanceInformation",
          "cloudwatch:PutMetricData",
          "ds:CreateComputer",
          "ds:DescribeDirectories",
          "ec2:DescribeInstanceStatus",
          "logs:*",
          "ec2messages:*"
        ],
        Resource  = "*"
      },
      {
        Effect = "Allow",
        Action = "iam:CreateServiceLinkedRole",
        Resource = "arn:aws:iam::*:role/aws-service-role/ssm.amazonaws.com/AWSServiceRoleForAmazonSSM*",
        Condition = {
          StringLike = {
            "iam:AWSServiceName" = "ssm.amazonaws.com"
          }
        }
      },
      {
        Effect = "Allow",
        Action = [
          "iam:DeleteServiceLinkedRole",
          "iam:GetServiceLinkedRoleDeletionStatus"
        ],
        Resource = "arn:aws:iam::*:role/aws-service-role/ssm.amazonaws.com/AWSServiceRoleForAmazonSSM*"
      },
      {
        Effect = "Allow",
        Action = [
          "ssmmessages:CreateControlChannel",
          "ssmmessages:CreateDataChannel",
          "ssmmessages:OpenControlChannel",
          "ssmmessages:OpenDataChannel"
        ],
        Resource = "*"
      }
    ]
  })
}

# IAM Role for Systems Manager
resource "aws_iam_role" "ssm_role" {
  name               = "SSMRole"
  assume_role_policy = jsonencode({
    Version   = "2012-10-17",
    Statement = [{
      Effect    = "Allow",
      Principal = {
        Service = "ec2.amazonaws.com"
      },
      Action    = "sts:AssumeRole"
    }]
  })
}

# Attach the IAM Policy to the IAM Role
resource "aws_iam_role_policy_attachment" "ssm_attach" {
  role       = aws_iam_role.ssm_role.name
  policy_arn = aws_iam_policy.ssm_policy.arn
}

resource "aws_iam_instance_profile" "ssm_profile" {
  name = "SSMInstanceProfile"
  role = aws_iam_role.ssm_role.name  # Associate the IAM role directly with the instance profile
}

```

**Step 8:- Create a file for  Pem-key and**

* Create kay_pair.tf file and add the below code to it

```
# crrateing tls_private_key
resource "tls_private_key" "rsa-4096-example" {
        algorithm = "RSA"
        rsa_bits  = 4096
}

# key_pair
resource "aws_key_pair" "my_keypair" {
        key_name   = "my-keypair"
        public_key = tls_private_key.rsa-4096-example.public_key_openssh
}

# storing key_pair to s3
resource "aws_s3_bucket_object" "private_key" {
        bucket = "myfstbcktposwal"
        key    = "my-keypair.pem"
        content = tls_private_key.rsa-4096-example.private_key_pem
}

```
**Step 9:- Create a file for EC2 instances and**

* Create ec2.tf file and add the below code to it

  ```
  # Creating 1st EC2 instance in Public Subnet
  resource "aws_instance" "demoinstance" {
    ami                         = "ami-087c17d1fe0178315"
    instance_type               = "t2.micro"
    count                       = 1
    key_name                    = "tests"
    vpc_security_group_ids      = ["${aws_security_group.demosg.id}"]
    subnet_id                   = "${aws_subnet.demoinstance.id}"
    associate_public_ip_address = true
    user_data                   = "${file("data.sh")}"
  tags = {
    Name = "My Public Instance"
  }
  }
  # Creating 2nd EC2 instance in Public Subnet
  resource "aws_instance" "demoinstance1" {
    ami                         = "ami-087c17d1fe0178315"
    instance_type               = "t2.micro"
    count                       = 1
    key_name                    = "tests"
    vpc_security_group_ids      = ["${aws_security_group.demosg.id}"]
    subnet_id                   = "${aws_subnet.demoinstance.id}"
    associate_public_ip_address = true
    user_data                   = "${file("data.sh")}"
  tags = {
    Name = "My Public Instance 2"
  }
  }
  ```

* I have used the userdata to configure the EC2 instance, I will discuss data.sh file later in the article

**Step 10:- Create a file for Security Group for the FrontEnd tier**

* Create web_sg.tf file and add the below code to it

  ```
  # Creating Security Group 
  resource "aws_security_group" "demosg" {
    vpc_id = "${aws_vpc.demovpc.id}"
  # Inbound Rules
  # HTTP access from anywhere
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # HTTPS access from anywhere
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # Outbound Rules
  # Internet access to anywhere
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "Web SG"
  }
  }
  ```

* I have opened 80,443 & 22 ports for the inbound connection and I have opened all the ports for the outbound connection


**Step 11:- Create a file Application Load Balancer**

* Create alb.tf file and add the below code to it

  ```
  # Creating External LoadBalancer
  resource "aws_lb" "external-alb" {
    name               = "External LB"
    internal           = false
    load_balancer_type = "application"
    security_groups    = [aws_security_group.demosg.id]
    subnets            = [aws_subnet.public-subnet-1.id, aws_subnet.public-subnet-1.id]
  }
  resource "aws_lb_target_group" "target-elb" {
    name     = "ALB TG"
    port     = 80
    protocol = "HTTP"
    vpc_id   = aws_vpc.demovpc.id
  }
  resource "aws_lb_target_group_attachment" "attachment" {
    target_group_arn = aws_lb_target_group.external-alb.arn
    target_id        = aws_instance.demoinstance.id
    port             = 80
  depends_on = [
    aws_instance.demoinstance,
  ]
  }
  resource "aws_lb_target_group_attachment" "attachment" {
    target_group_arn = aws_lb_target_group.external-alb.arn
    target_id        = aws_instance.demoinstance1.id
    port             = 80
  depends_on = [
    aws_instance.demoinstance1,
  ]
  }
  resource "aws_lb_listener" "external-elb" {
    load_balancer_arn = aws_lb.external-alb.arn
    port              = "80"
    protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.external-alb.arn
  }
  }
  ```
* The above load balancer is of type external
* Load balancer type is set to application
* The aws_lb_target_group_attachment resource will attach our instances to the Target Group.
* The load balancer will listen requests on port 80


**Step 10:- Create a file for outputs**

* Create outputs.tf file and add the below code to it

  ```
  # Getting the DNS of load balancer
  output "lb_dns_name" {
    description = "The DNS name of the load balancer"
    value       = "${aws_lb.external-alb.dns_name}"
  }
  ```
  
* From the above code, I will get the DNS of the application load balancer.

**Step 11:- Create a file for variable**

* Create vars.tf file and add the below code to it

  ```
  # Defining CIDR Block for VPC
  variable "vpc_cidr" {
    default = "10.0.0.0/16"
  }
  # Defining CIDR Block for 1st Subnet
  variable "subnet1_cidr" {
    default = "10.0.1.0/24"
  }
  # Defining CIDR Block for 2nd Subnet
  variable "subnet2_cidr" {
    default = "10.0.2.0/24"
  }
  
  ```


So, now our entire code is ready. We need to run the below steps to create infrastructure.

* terraform init is to initialize the working directory and downloading plugins of the provider
* terraform plan is to create the execution plan for our code
* terraform apply is to create the actual infrastructure. It will ask you to provide the Access Key and Secret Key in order to create the infrastructure. So, instead of hardcoding the Access Key and Secret Key, it is better to apply at the run time.


**Step 13:- Verify the resources**

* Terraform will create below resources

  * VPC ,Subnet, Route Table, Internet gatway, 
  * Application Load Balancer, Listner, Target group  *  
  * EC2 instances   
  * Security Groups 
  * IAM Poliy, IAM Role
  * Pem-Key

Once the resource creation finishes you can get the DNS of a load balancer and paste it into the browser and you can see load balancer will send the request to two instances.



 
