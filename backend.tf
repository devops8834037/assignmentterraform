terraform {
  backend "s3" {
    bucket         = "myfstbcktposwal"
    key            = "terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terraform-lock-table"
  }
}