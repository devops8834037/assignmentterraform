resource "aws_instance" "demoinstance" {
  ami                         = "ami-0ed59c68b90b204a8"
  instance_type               = "t2.micro"
  key_name                    = aws_key_pair.my_keypair.key_name
  vpc_security_group_ids      = ["${aws_security_group.demosg.id}"]
  subnet_id                   = "${aws_subnet.public-subnet-1.id}"
  associate_public_ip_address = true

  iam_instance_profile = aws_iam_instance_profile.ssm_profile.name  # Attach IAM role to EC2 instance

  tags = {
    Name = "My Public Instance-1"
  }
  
  depends_on = [aws_iam_instance_profile.ssm_profile]  # Ensure IAM resources are created before attaching to instance
}

# EC2 Instance 2
resource "aws_instance" "demoinstance1" {
  ami                         = "ami-0ed59c68b90b204a8"
  instance_type               = "t2.micro"
  key_name                    = aws_key_pair.my_keypair.key_name
  vpc_security_group_ids      = ["${aws_security_group.demosg.id}"]
  subnet_id                   = "${aws_subnet.public-subnet-2.id}"
  associate_public_ip_address = true

  iam_instance_profile = aws_iam_instance_profile.ssm_profile.name  # Attach IAM role to EC2 instance

  tags = {
    Name = "My Public Instance-2"
  }
  
  depends_on = [aws_iam_instance_profile.ssm_profile]  # Ensure IAM resources are created before attaching to instance
}

# Outputs
output "ssm_profile_name" {
  value = aws_iam_instance_profile.ssm_profile.name
}