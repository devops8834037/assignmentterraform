# crrateing tls_private_key
resource "tls_private_key" "rsa-4096-example" {
        algorithm = "RSA"
        rsa_bits  = 4096
}

# key_pair
resource "aws_key_pair" "my_keypair" {
        key_name   = "my-keypair"
        public_key = tls_private_key.rsa-4096-example.public_key_openssh
}

# storing key_pair to s3
resource "aws_s3_bucket_object" "private_key" {
        bucket = "myfstbcktposwal"
        key    = "my-keypair.pem"
        content = tls_private_key.rsa-4096-example.private_key_pem
}
